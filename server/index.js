const storage = require('node-persist');
const Koa = require('koa');
var bodyParser = require('koa-bodyparser');
var Router = require('koa-router');

const port = 8000;
const KEY_COUNTER = 'counter';
const ITEM_PREFIX = 'item_';

const app = new Koa();
var router = new Router();

(async () => {
  await storage.init({
    dir: 'storage',
    expiredInterval: 1000 * 60 * 60 * 24 * 7
  });
})();

router
  .param('id', async (id, ctx, next) => {
    if ((await storage.keys()).includes(ITEM_PREFIX + id)) { // if item is found from storage
      ctx.item = await storage.getItem(ITEM_PREFIX + id); // get item
      return next();
    } else {
      return ctx.status = 404; // if item not found, return 404 (not found)
    }
  })
  .get('/items', async (ctx, next) => {
    let allPostIts = await storage.valuesWithKeyMatch(ITEM_PREFIX); // get all post its from storage which start with item_ prefix
    ctx.response.body = JSON.stringify(allPostIts); // return them all in the response
    return ctx.status = 200;
  })
  .post('/items', async (ctx, next) => {
    var postIt = ctx.request.body; // new post-it is in request body
    postIt.likes = 0; // add new property "likes"
    let counter = '0'; // item counter starting value
    if ((await storage.keys()).includes(KEY_COUNTER)) { // if existing counter is already in storage
      counter = await storage.getItem(KEY_COUNTER); // use if found
    }
    let updatedCounter = (Number(counter) + 1).toString(); // increase counter to get new unique item id
    postIt.id = updatedCounter; // attach id as property to item
    await storage.setItem(ITEM_PREFIX + updatedCounter, postIt); // save item to storage
    await storage.setItem(KEY_COUNTER, updatedCounter); // save updated counter to storage

    ctx.response.body = JSON.stringify(postIt); // send newly created post it to storage
    return ctx.status = 200;
  })
  .patch('/items/:id', async (ctx, next) => {
    var postIt = ctx.request.body; // patched post-it is in request body
    if (postIt.text !== undefined) { // if text parameter is found from patch parameters
      ctx.item.text = postIt.text; // add it to the item to patch
    }

    await storage.updateItem(ITEM_PREFIX + ctx.item.id, ctx.item); // update item in storage
    ctx.response.body = JSON.stringify(ctx.item); // send updated item in response
    return ctx.status = 200;
  })
  .del('/items/:id', async (ctx, next) => {
    await storage.removeItem(ITEM_PREFIX + ctx.item.id); // remove item
    return ctx.status = 200;
  })
  .post('/items/:id/like', async (ctx, next) => {
    ctx.item.likes = ctx.item.likes + 1; // increase likes of item by one
    await storage.updateItem(ITEM_PREFIX + ctx.item.id, ctx.item); // update item in storage
    ctx.response.body = JSON.stringify(ctx.item); // send updated item in response
    return ctx.status = 200;
  })
  .del('/items/:id/like', async (ctx, next) => {
    if (ctx.item.likes > 0) { // if item has any existing likes
      ctx.item.likes = ctx.item.likes - 1; // decrease likes of item by one
    }

    await storage.updateItem(ITEM_PREFIX + ctx.item.id, ctx.item); // update item in storage
    ctx.response.body = JSON.stringify(ctx.item); // send updated item in response
    return ctx.status = 200;
  })

app.use(bodyParser());
app.use(router.routes());

app.listen(port, () => {
  console.log("App running in port: " + port);
});
