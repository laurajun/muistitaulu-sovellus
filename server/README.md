# Muistitaulu/Pinboard project - Mimmit koodaa 2020

Pinboard application for the Mimmit koodaa 2020 presentation.

## Pre-install

- Node version: v.14.15.1
- Make folder named "storage" for node-persist database

## Installation

    npm install node-persist koa koa-router koa-bodyparser

## Running the application

    node index.js
