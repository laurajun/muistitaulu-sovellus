import React from 'react';
import PropTypes from 'prop-types';

import Button from './Button';
import IconPlus from './assets/plus.svg';

const Nav = (props) => {
    return (
        <div className="top-container">
            <Button onClick={props.onAddNewPostIt}>
                <img src={IconPlus} alt="" />
                Add new note
            </Button>
        </div>
    );
};

Nav.propTypes = {
    onAddNewPostIt: PropTypes.func.isRequired,

};

export default Nav;
