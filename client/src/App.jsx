import React from 'react';

import './App.css';
import Duck from './assets/duck.svg';
import Logo from './assets/vincit.svg';
import MimmitKoodaa from './assets/mimmitkoodaa.svg';
import Nav from './Nav';
import Main from './Main';

const App = () => {
    const [postIts, setPostIts] = React.useState([]);

    React.useEffect(() => {
        fetch('items')
            .then((response) => response.json())
            .then((data) => setPostIts(data.sort((a, b) => a.id - b.id)));
    }, []);

    const createPostIt = (data) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
        };
        fetch('items', requestOptions)
            .then((response) => response.json())
            .then((data) => {
                const newNotes = [...postIts, data];
                setPostIts(newNotes.sort((a, b) => a.id - b.id));
            });
    };

    const likePostIt = (id) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
        };
        fetch(`items/${id}/like`, requestOptions)
            .then((response) => response.json())
            .then((data) => {
                setPostIts(postIts.map((n) => (n.id === id ? data : n)));
            });
    };

    const updatePostIt = (id, data) => {
        const requestOptions = {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
        };
        fetch(`items/${id}`, requestOptions)
            .then((response) => response.json())
            .then((data) => {
                setPostIts(postIts.map((n) => (n.id === id ? data : n)));
            });
    };

    const deletePostIt = (id) => {
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        };
        fetch(`items/${id}`, requestOptions).then(() => {
            setPostIts(postIts.filter((n) => n.id !== id));
        });
    };

    const handleCreatePostIt = () => {
        createPostIt({ likes: 0, text: '' });
    };

    return (
        <div className="app">
            <Nav onAddNewPostIt={handleCreatePostIt} />

            <Main
                postIts={postIts}
                onDeletePost={deletePostIt}
                onLikePost={likePostIt}
                onUpdatePost={updatePostIt}
            />

            <div className="bottom-container">
                <div className="logos">
                    <img src={Logo} alt="Vincit" />
                    <img src={MimmitKoodaa} alt="Mimmit koodaa" />
                </div>
                <img
                    src={Duck}
                    className="duck"
                    alt="Black duck with a headband"
                />
            </div>
        </div>
    );
};

export default App;
