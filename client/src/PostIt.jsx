import React from 'react';
import PropTypes from 'prop-types';

import './PostIt.css';
import Heart from './assets/heart.svg';
import HeartFilled from './assets/heart-filled.svg';
import IconDelete from './assets/delete.svg';

const PostIt = (props) => {
    const [text, setText] = React.useState(props.text);
    const [pulsing, setPulsing] = React.useState(false);

    React.useEffect(() => {
        setText(props.text);
    }, [props]);

    const handleTextChange = (e) => {
        setText(e.currentTarget.value);
    };

    const handleTextSave = (e) => {
        props.onChange(props.id, { text: e.currentTarget.value });
    };

    const handleAddLike = () => {
        props.onLike(props.id);
    };

    const handleDelete = () => {
        props.onDelete(props.id);
    };

    return (
        <div className="postit">
            <button className="delete" onClick={handleDelete}>
                <img src={IconDelete} alt="Delete" />
            </button>
            <textarea
                onChange={handleTextChange}
                onBlur={handleTextSave}
                value={text}
            />

            <button
                className="like"
                onClick={() => {
                    setPulsing(true);
                    handleAddLike();
                }}
                onAnimationEnd={() => {
                    setPulsing(false);
                }}
            >
                <img
                    className={pulsing ? 'pulse' : ''}
                    src={Heart}
                    alt="Heart"
                    onMouseOver={(e) => (e.currentTarget.src = HeartFilled)}
                    onMouseLeave={(e) => (e.currentTarget.src = Heart)}
                    style={{ width: Math.min(props.likes, 10) + 24 }}
                />
                <span className="likes-count">{props.likes}</span>
            </button>
        </div>
    );
};

PostIt.propTypes = {
    id: PropTypes.number,
    likes: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onLike: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
};

export default PostIt;
